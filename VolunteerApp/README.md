1. docker pull postgres
2. sudo docker run --name db -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d postgres
3. Using docker exec -it db bash and then psql -h localhost -p 5432 -U postgres you can see everything stored in db using regular sql commands.

How to generate Requests:
POST http://localhost:8080/volunteerUsers
Content-Type: application/json

{
  "username": "yakunole",
  "firstName": "Oleksandr",
  "lastName": "Yakunin",
  "country": "Ukraine"
}

then you can use GET http://localhost:8080/volunteerUsers/{id}, GET http://localhost:8080/volunteerUsers(for list of all users) and DELETE http://localhost:8080/volunteerUsers/{id}

Similar follows for all other requests


