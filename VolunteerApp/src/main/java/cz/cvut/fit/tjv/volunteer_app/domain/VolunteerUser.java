package cz.cvut.fit.tjv.volunteer_app.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
@Entity
@Getter
@Setter
public  class VolunteerUser implements AbstractUser, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String username;

    private String firstName;
    private String lastName;
    private Country country;

    private LocalDateTime dateOfCreation = LocalDateTime.now();
    @ManyToMany
//    @JoinTable(name = "user_friend",
//            joinColumns = @JoinColumn(name = "volunteer_user_id"),
//            inverseJoinColumns = @JoinColumn(name = "volunteer_user_friend_id")
//    )
    private Set<VolunteerUser> userFriends = new HashSet<>();
    private Integer numOfWorksDone = 0;
//    @JoinTable(name = "user_friend",
//            joinColumns = @JoinColumn(name = "volunteer_user_id"),
//            inverseJoinColumns = @JoinColumn(name = "volunteer_user_friend_id")
//    )
    @ManyToMany
    private Set<HelpRequestPost> providedHelps = new HashSet<>();

    public VolunteerUser() {}

    public VolunteerUser(Long id, String username, String firstName, String lastName, Country country) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public VolunteerUser(String username, String firstName, String lastName, Country country) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public void addProvidedWork(HelpRequestPost post) {
        providedHelps.add(Objects.requireNonNull(post));
    }

    public Integer getNumOfProvidedWorks() { return providedHelps.size(); }

    @Override
    public void addFriend(VolunteerUser user) {
        userFriends.add(Objects.requireNonNull(user));
    }

    @Override
    public void changeUsername(String newUsername) {
        username = Objects.requireNonNull(newUsername);
    }

    @Override
    public void changeCountry(Country newCountry) {
        country = Objects.requireNonNull(newCountry);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        VolunteerUser user = (VolunteerUser) obj;

        return getId().equals(user.getId());
    }

    @Serial
    private static final long serialVersionUID = 6529685098267757690L;
}

