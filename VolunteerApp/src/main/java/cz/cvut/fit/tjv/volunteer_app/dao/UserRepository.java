package cz.cvut.fit.tjv.volunteer_app.dao;

import cz.cvut.fit.tjv.volunteer_app.domain.AbstractUser;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;

public interface UserRepository {
    void changeCountry(Long id, Country country);
    void changeUsername(Long id, String newUsername);
    void addFriend(Long volunteerId, VolunteerUser newFriend);
}
