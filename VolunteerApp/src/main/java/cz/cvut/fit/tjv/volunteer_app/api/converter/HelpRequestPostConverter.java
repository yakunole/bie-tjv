package cz.cvut.fit.tjv.volunteer_app.api.converter;


import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.HelpRequestPostDto;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;

public class HelpRequestPostConverter {
    public static HelpRequestPost toModel(HelpRequestPostDto helpRequestPostDto) {
        return new HelpRequestPost(helpRequestPostDto.message);
    }

    public static HelpRequestPostDto fromModel(HelpRequestPost helpRequestPost) {
        return new HelpRequestPostDto(helpRequestPost.getContent(), helpRequestPost.getAuthorOfPost().getId());
    }
}
