package cz.cvut.fit.tjv.volunteer_app.api.controller;

public class EntityCanNotBeCreatedException extends RuntimeException {
    public EntityCanNotBeCreatedException(String s) {
        super(s);
    }
}
