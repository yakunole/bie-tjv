package cz.cvut.fit.tjv.volunteer_app.dao.jpa;

import cz.cvut.fit.tjv.volunteer_app.dao.AbstractCrudRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.UserRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpReceiverUser;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Repository
public interface HelpReceiverUserJpaRepository extends JpaRepository<HelpReceiverUser, Long>, AbstractCrudRepository<Long, HelpReceiverUser>, UserRepository {

    default void addHelpRequest(Long id, HelpRequestPost post) {
        Optional<HelpReceiverUser> helpReceiverUser = readById(id);
        if(helpReceiverUser.isPresent()) {
            var user = helpReceiverUser.get();
            user.addHelpRequest(post);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void changeCountry(Long id, Country newCountry) {
        Optional<HelpReceiverUser> helpReceiverUser = readById(id);
        if(helpReceiverUser.isPresent()) {
            var user = helpReceiverUser.get();
            user.changeCountry(newCountry);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void changeUsername(Long id, String newUsername) {
        Optional<HelpReceiverUser> helpReceiverUser = readById(id);
        if(helpReceiverUser.isPresent()) {
            var user = helpReceiverUser.get();
            user.changeUsername(newUsername);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void addFriend(Long id, VolunteerUser newFriend) {
        Optional<HelpReceiverUser> helpReceiverUser = readById(id);
        if(helpReceiverUser.isPresent()) {
            var user = helpReceiverUser.get();
            user.addFriend(newFriend);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void createOrUpdate(HelpReceiverUser entity) {
        this.save(entity);
    }

    @Override
    default Optional<HelpReceiverUser> readById(Long id) {
        return this.findById(id);
    }

    @Override
    default Collection<HelpReceiverUser> readAll() {
        return this.findAll();
    }

    @Override
    default boolean exists(HelpReceiverUser entity) {
        return readById(entity.getId()).isPresent();
    }
    @Override
    default boolean existsById(Long id) {
        return readById(Objects.requireNonNull(id)).isPresent();
    }
}
