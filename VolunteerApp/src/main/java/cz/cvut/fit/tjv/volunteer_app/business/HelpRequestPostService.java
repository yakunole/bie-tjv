package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.dao.file.HelpRequestPostFileRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.jpa.HelpRequestPostJpaRepository;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.stereotype.Component;

@Component
public class HelpRequestPostService extends AbstractCrudService<Long, HelpRequestPost> implements PostService<String>{
    private final HelpRequestPostJpaRepository helpRequestPostJpaRepository;

    public HelpRequestPostService(HelpRequestPostJpaRepository repository) {
        super(repository);
        this.helpRequestPostJpaRepository = repository;
    }

    public void addVolunteer(Long id, VolunteerUser volunteerUser) {
        helpRequestPostJpaRepository.addVolunteer(id, volunteerUser);
    }

    @Override
    public void changeContent(Long id, String newContent) {
        helpRequestPostJpaRepository.changeContent(id, newContent);
    }

    @Override
    public void addLike(Long id, VolunteerUser user) {
        helpRequestPostJpaRepository.addLike(id, user);
    }
}
