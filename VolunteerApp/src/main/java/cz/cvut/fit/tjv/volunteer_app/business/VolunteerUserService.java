package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.dao.jpa.VolunteerUserJpaRepository;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.stereotype.Component;

@Component
public class VolunteerUserService extends AbstractCrudService<Long, VolunteerUser> implements UserService {
    private final VolunteerUserJpaRepository volunteerUserJpaRepository;

    public VolunteerUserService(VolunteerUserJpaRepository repository) {
        super(repository);
        this.volunteerUserJpaRepository = repository;
    }

    public void addProvidedWork(Long id, HelpRequestPost post) {
        volunteerUserJpaRepository.addProvidedWork(id, post);
    }

    @Override
    public void changeCountry(Long id, Country country) {
        volunteerUserJpaRepository.changeCountry(id, country);
    }

    @Override
    public void changeUsername(Long id, String newUsername) {
        volunteerUserJpaRepository.changeUsername(id, newUsername);
    }

    @Override
    public void addFriend(Long id, VolunteerUser newFriend) {
        volunteerUserJpaRepository.addFriend(id, newFriend);
    }
}
