package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.dao.AbstractCrudRepository;

import java.util.Collection;
import java.util.Optional;

public abstract class AbstractCrudService<K, E> {
    protected final AbstractCrudRepository<K, E> repository;

    protected AbstractCrudService(AbstractCrudRepository<K, E> repository) {
        this.repository = repository;
    }

    public void create(E entity) {
        repository.createOrUpdate(entity);
    }

    public Optional<E> readById(K id) {
        return repository.readById(id);
    }

    public Collection<E> readAll() { return repository.readAll(); }

    public void update(E entity) {
            repository.createOrUpdate(entity);
    }

    public void deleteById(K id) { repository.deleteById(id); }
}
