package cz.cvut.fit.tjv.volunteer_app.dao.jpa;

import cz.cvut.fit.tjv.volunteer_app.dao.AbstractCrudRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.PostRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Repository
public interface HelpRequestPostJpaRepository extends JpaRepository<HelpRequestPost, Long>, AbstractCrudRepository<Long, HelpRequestPost>, PostRepository {

    default void addVolunteer(Long id, VolunteerUser user) {
        Optional<HelpRequestPost> helpRequestPost = readById(id);
        if(helpRequestPost.isPresent()) {
            var post = helpRequestPost.get();
            post.addVolunteer(user);
            createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void addLike(Long id, VolunteerUser user) {
        Optional<HelpRequestPost> helpRequestPost = readById(id);
        if(helpRequestPost.isPresent()) {
            var post = helpRequestPost.get();
            post.addLike(user);
            createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void changeContent(Long id, String newContent) {
        Optional<HelpRequestPost> helpRequestPost = readById(id);
        if(helpRequestPost.isPresent()) {
            var post = helpRequestPost.get();
            post.changeContent(newContent);
            createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void createOrUpdate(HelpRequestPost entity) {
        this.save(entity);
    }

    @Override
    default Optional<HelpRequestPost> readById(Long id) {
        return this.findById(id);
    }

    @Override
    default Collection<HelpRequestPost> readAll() {
        return this.findAll();
    }

    @Override
    default boolean exists(HelpRequestPost entity) {
        return readById(entity.getId()).isPresent();
    }

    @Override
    default boolean existsById(Long id) {
        return readById(Objects.requireNonNull(id)).isPresent();
    }
}
