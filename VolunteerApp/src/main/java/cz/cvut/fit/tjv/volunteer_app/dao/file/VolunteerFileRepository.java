package cz.cvut.fit.tjv.volunteer_app.dao.file;

import cz.cvut.fit.tjv.volunteer_app.dao.UserRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class VolunteerFileRepository extends AbstractFileRepository<Long, VolunteerUser> implements UserRepository {

    protected VolunteerFileRepository() {
        super(Path.of("volunteers.bin"));
    }

    @Override
    public void changeCountry(Long id, Country country) {
        if(super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.changeCountry(country);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    public void changeUsername(Long id, String newUsername) {
        if(super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.changeUsername(newUsername);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    public void addFriend(Long volunteerId, VolunteerUser newFriend) {
        if(super.readById(volunteerId).isPresent()) {
            var user = super.readById(volunteerId).get();
            user.addFriend(newFriend);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    public void addDoneWork(Long id, HelpRequestPost post) {
        if(super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.addProvidedWork(post);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    protected Long entityId(VolunteerUser entity) {
        return entity.getId();
    }
    @Override
    public boolean existsById(Long id) {
        return readById(id).isPresent();
    }
}
