package cz.cvut.fit.tjv.volunteer_app.domain;

public interface AbstractUser {
    void addFriend(VolunteerUser user);
    void changeUsername(String newUsername);
    void changeCountry(Country newCountry);
}