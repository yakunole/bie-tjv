package cz.cvut.fit.tjv.volunteer_app.dao;

import cz.cvut.fit.tjv.volunteer_app.domain.AbstractUser;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;

public interface PostRepository {
    void changeContent(Long id, String newContent);
    void addLike(Long id, VolunteerUser user);
}
