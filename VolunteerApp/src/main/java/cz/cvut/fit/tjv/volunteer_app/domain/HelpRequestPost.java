package cz.cvut.fit.tjv.volunteer_app.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
public class HelpRequestPost implements AbstractPost<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private  Long id;
    @ManyToOne
//    @JoinColumn(name = "author_help_receiver_user_id", nullable = false)
    private HelpReceiverUser authorOfPost;
    private LocalDateTime dateOfCreation = LocalDateTime.now();

    private String postMessage;

//    @JoinTable(name = "user_who_like_post",
//                joinColumns = @JoinColumn(name = "help_request_post_id"),
//                inverseJoinColumns = @JoinColumn(name = "volunteer_user_id")
//    )
    @OneToMany
    private Set<VolunteerUser> usersWhoLikedPost = new HashSet<>();
    @ManyToMany
//    @JoinTable(name = "volunteer_willing_to_help",
//            joinColumns = @JoinColumn(name = "help_request_post_id"),
//            inverseJoinColumns = @JoinColumn(name = "volunteer_user_id")
//
//    )
    private Set<VolunteerUser> volunteersWillingToHelp = new HashSet<>();

    public HelpRequestPost() {}

    public HelpRequestPost(HelpReceiverUser authorOfPost, String postMessage) {
        this.authorOfPost = authorOfPost;
        this.postMessage = postMessage;
    }

    public HelpRequestPost(String postMessage) {
        this.authorOfPost = null;
        this.postMessage = postMessage;
    }

    public void addVolunteer(VolunteerUser user) {
        volunteersWillingToHelp.add(Objects.requireNonNull(user));
    }

    @Override
    public String getContent() {
        return postMessage;
    }

    @Override
    public void changeContent(String newContent) {
        postMessage = newContent;
    }

    @Override
    public void addLike(VolunteerUser user) {
        usersWhoLikedPost.add(Objects.requireNonNull(user));
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        HelpRequestPost post = (HelpRequestPost) obj;

        return getId().equals(post.getId());
    }
}
