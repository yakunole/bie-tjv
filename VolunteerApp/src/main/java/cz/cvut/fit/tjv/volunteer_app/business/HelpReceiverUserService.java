package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.dao.jpa.HelpReceiverUserJpaRepository;
import cz.cvut.fit.tjv.volunteer_app.domain.*;
import org.springframework.stereotype.Component;

@Component
public class HelpReceiverUserService extends AbstractCrudService<Long, HelpReceiverUser> implements UserService {
    private final HelpReceiverUserJpaRepository helpReceiverUserJpaRepository;

    public HelpReceiverUserService(HelpReceiverUserJpaRepository repository) {
        super(repository);
        helpReceiverUserJpaRepository = repository;
    }

    public void addHelpRequest(Long id, HelpRequestPost post) {
        helpReceiverUserJpaRepository.addHelpRequest(id, post);
    }

    @Override
    public void changeCountry(Long id, Country country) {
        helpReceiverUserJpaRepository.changeCountry(id, country);
    }

    @Override
    public void changeUsername(Long id, String newUsername) {
        helpReceiverUserJpaRepository.changeUsername(id, newUsername);
    }

    @Override
    public void addFriend(Long id, VolunteerUser newFriend) {
        helpReceiverUserJpaRepository.addFriend(id, newFriend);
    }
}
