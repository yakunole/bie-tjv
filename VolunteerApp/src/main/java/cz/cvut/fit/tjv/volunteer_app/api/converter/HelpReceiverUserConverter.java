package cz.cvut.fit.tjv.volunteer_app.api.converter;


import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.HelpReceiverUserDto;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpReceiverUser;

public class HelpReceiverUserConverter {

    public static HelpReceiverUser toModel(HelpReceiverUserDto helpReceiverUserDto) {
        return  new HelpReceiverUser(helpReceiverUserDto.id,
                helpReceiverUserDto.username,
                helpReceiverUserDto.firstName,
                helpReceiverUserDto.lastName,
                Country.valueOf(helpReceiverUserDto.country));
    }

    public static HelpReceiverUserDto fromModel(HelpReceiverUser helpReceiverUser) {
        return new HelpReceiverUserDto(helpReceiverUser.getId(),
                helpReceiverUser.getUsername(),
                helpReceiverUser.getFirstName(),
                helpReceiverUser.getLastName(),
                helpReceiverUser.getCountry().getCountryName());
    }
}
