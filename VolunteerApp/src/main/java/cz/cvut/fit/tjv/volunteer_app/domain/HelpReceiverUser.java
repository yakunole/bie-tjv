package cz.cvut.fit.tjv.volunteer_app.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Getter
@Setter
public class HelpReceiverUser implements AbstractUser {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String username;

    private  String firstName;
    private  String lastName;
    private Country country;

    private  LocalDateTime dateOfCreation = LocalDateTime.now();
    @ManyToMany
    private  Set<VolunteerUser> userFriends = new HashSet<>();
    @OneToMany(mappedBy = "authorOfPost")
    private  Set<HelpRequestPost> helpRequestPosts = new HashSet<>();

    public HelpReceiverUser() {}

    public HelpReceiverUser(String username, String firstName, String lastName, Country country) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public HelpReceiverUser(Long id, String username, String firstName, String lastName, Country country) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public void addHelpRequest(HelpRequestPost post) {
        helpRequestPosts.add(Objects.requireNonNull(post));
    }

    @Override
    public void addFriend(VolunteerUser user) {
        userFriends.add(Objects.requireNonNull(user));
    }

    @Override
    public void changeUsername(String newUsername) {
        username = Objects.requireNonNull(newUsername);
    }

    @Override
    public void changeCountry(Country newCountry) {
        country = Objects.requireNonNull(newCountry);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        HelpReceiverUser user = (HelpReceiverUser) obj;

        return getId().equals(user.getId());
    }
}
