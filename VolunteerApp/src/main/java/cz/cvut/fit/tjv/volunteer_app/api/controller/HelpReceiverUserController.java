package cz.cvut.fit.tjv.volunteer_app.api.controller;

import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.HelpReceiverUserDto;
import cz.cvut.fit.tjv.volunteer_app.api.converter.HelpReceiverUserConverter;
import cz.cvut.fit.tjv.volunteer_app.api.converter.VolunteerUserConverter;
import cz.cvut.fit.tjv.volunteer_app.business.HelpReceiverUserService;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpReceiverUser;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
public class HelpReceiverUserController {
    private final HelpReceiverUserService helpReceiverUserService;

    public HelpReceiverUserController(HelpReceiverUserService helpReceiverUserService) {
        this.helpReceiverUserService = helpReceiverUserService;
    }

    @PostMapping("/helpReceiverUsers")
    HelpReceiverUserDto createHelpReceiverUser(@RequestBody HelpReceiverUserDto helpReceiverUserDto) {
        if(helpReceiverUserDto.getId() != null)
            throw new EntityCanNotBeCreatedException("Entity with specified id can not be created");
        var helpReceiverUser = HelpReceiverUserConverter.toModel(helpReceiverUserDto);

        helpReceiverUserService.create(helpReceiverUser);
        helpReceiverUser = helpReceiverUserService.readById(helpReceiverUser.getId()).orElseThrow(
                () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
        );

        return helpReceiverUserDto;
    }

    @GetMapping("/helpReceiverUsers")
    Collection<HelpReceiverUser> getAllHelpReceiverUsers() {
        return helpReceiverUserService.readAll();
    }

    @GetMapping("/helpReceiverUsers/{id}")
    HelpReceiverUserDto getHelpReceiverUserById(@PathVariable Long id) {
        return HelpReceiverUserConverter.fromModel(helpReceiverUserService.readById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
                )
        );
    }

    @PutMapping("/helpReceiverUsers/{id}")
    HelpReceiverUserDto updateHelpReceiverUser(@RequestBody HelpReceiverUserDto helpReceiverUserDto, @PathVariable Long id) {
        helpReceiverUserService.readById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
                );


        HelpReceiverUser helpReceiverUser = HelpReceiverUserConverter.toModel(helpReceiverUserDto);

        helpReceiverUserService.update(helpReceiverUser);
        return helpReceiverUserDto;

    }

    @DeleteMapping("/helpReceiverUsers/{id}")
    void deleteHelpReceiverUser(@PathVariable Long id) {
        if(helpReceiverUserService.readById(id).isEmpty())
            throw new NoSuchEntityInRepositoryException("Entity with such id is not present");

        helpReceiverUserService.deleteById(id);
    }

}
