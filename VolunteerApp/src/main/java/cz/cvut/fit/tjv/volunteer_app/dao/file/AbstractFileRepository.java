package cz.cvut.fit.tjv.volunteer_app.dao.file;

import cz.cvut.fit.tjv.volunteer_app.dao.AbstractCrudRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.FileDbCreatingException;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.FileDbIoException;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.MissingClassException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public abstract class AbstractFileRepository<K, E> implements AbstractCrudRepository<K, E> {

    private final Path fileDatabasePath;
    private final Map<K, E> inMemoryDb = new HashMap<>();

    protected AbstractFileRepository(Path fileDatabasePath) {
        this.fileDatabasePath = Objects.requireNonNull(fileDatabasePath);
        if(!Files.exists(fileDatabasePath)) {
            try {
                Files.createFile(fileDatabasePath);
            } catch (IOException e) {
                throw new FileDbCreatingException(e);
            }
        }
    }

    private void loadFromFile() {
        inMemoryDb.clear();

        try (ObjectInputStream inputStream = new ObjectInputStream(Files.newInputStream(fileDatabasePath))) {
            Object readIn;
            if ((readIn = inputStream.readObject()) != null) {
                if (readIn instanceof Map mapFromFile) {
                    inMemoryDb.putAll(mapFromFile);
                }
            }
        } catch (EOFException e) {
            System.err.println("Finished reading from " + fileDatabasePath);
        } catch (ClassNotFoundException e) {
            throw new MissingClassException("Cannot load data from " + fileDatabasePath + ". Some class is missing or has a version mismatch.", e);
        } catch (IOException e) {

        }
    }

    private void loadToFile() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(Files.newOutputStream(fileDatabasePath))) {
            outputStream.writeObject(inMemoryDb);
        } catch (IOException e) {
            throw new FileDbIoException(e);
        }
    }

    public void createOrUpdate(E entity) {
        loadFromFile();
        inMemoryDb.put(entityId(entity), Objects.requireNonNull(entity));
        loadToFile();
    }

    public Optional<E> readById(K id) {
        loadFromFile();
        return Optional.ofNullable(inMemoryDb.get(id));
    }

    public Collection<E> readAll() {
        loadFromFile();
        return inMemoryDb.values();
    }

    public void deleteById(K id) {
        loadFromFile();
        inMemoryDb.remove(id);
        loadToFile();
    }

    public boolean exists(E entity) {
        loadFromFile();
        return inMemoryDb.containsKey(entityId(entity));
    }

    protected abstract K entityId(E entity);
}
