package cz.cvut.fit.tjv.volunteer_app.api.converter;

import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.VolunteerUserDto;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;

import java.util.ArrayList;
import java.util.Collection;

public class VolunteerUserConverter {
    public static VolunteerUser toModel(VolunteerUserDto userDto) {
        return new VolunteerUser(
                userDto.id,
                userDto.username,
                userDto.firstName,
                userDto.lastName,
                Country.valueOf(userDto.country));
    }

    public static VolunteerUserDto fromModel(VolunteerUser volunteerUser) {
        return new VolunteerUserDto(volunteerUser.getId(),
                volunteerUser.getUsername(),
                volunteerUser.getFirstName(),
                volunteerUser.getLastName(),
                volunteerUser.getCountry().getCountryName());
    }

    public static Collection<VolunteerUser> toModelMany(Collection<VolunteerUserDto> volunteerUserDtos) {
        Collection<VolunteerUser> users = new ArrayList<>();
        volunteerUserDtos.forEach((u) -> users.add(toModel(u)));
        return users;
    }

    public static Collection<VolunteerUserDto> fromModelMany(Collection<VolunteerUser> volunteerUsers) {
        Collection<VolunteerUserDto> userDtos = new ArrayList<>();
        volunteerUsers.forEach((u) -> userDtos.add(fromModel(u)));
        return userDtos;
    }
}
