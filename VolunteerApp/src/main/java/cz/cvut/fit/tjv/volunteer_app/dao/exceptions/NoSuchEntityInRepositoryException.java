package cz.cvut.fit.tjv.volunteer_app.dao.exceptions;

public class NoSuchEntityInRepositoryException extends RuntimeException {
    public NoSuchEntityInRepositoryException(String s) {
        super(s);
    }
}
