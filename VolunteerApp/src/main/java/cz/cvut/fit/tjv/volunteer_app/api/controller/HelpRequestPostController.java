package cz.cvut.fit.tjv.volunteer_app.api.controller;

import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.HelpRequestPostDto;
import cz.cvut.fit.tjv.volunteer_app.api.converter.HelpRequestPostConverter;
import cz.cvut.fit.tjv.volunteer_app.business.HelpReceiverUserService;
import cz.cvut.fit.tjv.volunteer_app.business.HelpRequestPostService;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
public class HelpRequestPostController {
    private final HelpRequestPostService helpRequestPostService;
    private final HelpReceiverUserService helpReceiverUserService;

    public HelpRequestPostController(HelpRequestPostService helpRequestPostService, HelpReceiverUserService helpReceiverUserService) {
        this.helpRequestPostService = helpRequestPostService;
        this.helpReceiverUserService = helpReceiverUserService;
    }

    @PostMapping("/helpRequestPosts")
    HelpRequestPostDto createHelpRequestPost(@RequestBody HelpRequestPostDto helpRequestPostDto) {

        if(helpRequestPostDto.getId() != null)
            throw new EntityCanNotBeCreatedException("Entity with specified id can not be created");

        HelpRequestPost helpRequestPost = HelpRequestPostConverter.toModel(helpRequestPostDto);

        var id = helpReceiverUserService.readById(helpRequestPostDto.getAuthorId());
        if(id.isPresent())
            helpRequestPost.setAuthorOfPost(id.get());
        else
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");

        helpRequestPostService.create(helpRequestPost);
        return helpRequestPostDto;
    }

    @GetMapping("/helpRequestPosts")
    Collection<HelpRequestPost> getAllHelpRequestPost() {
        return helpRequestPostService.readAll();
    }

    @GetMapping("/helpRequestPosts/{id}")
    HelpRequestPostDto getHelpRequestPostById(@PathVariable Long id) {
        return HelpRequestPostConverter.fromModel(helpRequestPostService.readById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
                )
        );
    }

    @DeleteMapping("/helpRequestPost/{id}")
    void deleteHelpRequestPost(@PathVariable Long id) {
        if(helpRequestPostService.readById(id).isEmpty())
            throw new NoSuchEntityInRepositoryException("Entity with such id is not present");
        helpRequestPostService.deleteById(id);
    }
}
