package cz.cvut.fit.tjv.volunteer_app.api.controller;

import cz.cvut.fit.tjv.volunteer_app.api.controller.dtos.VolunteerUserDto;
import cz.cvut.fit.tjv.volunteer_app.api.converter.VolunteerUserConverter;
import cz.cvut.fit.tjv.volunteer_app.business.VolunteerUserService;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
public class VolunteerUserController {
    private final VolunteerUserService volunteerUserService;

    VolunteerUserController(VolunteerUserService volunteerUserService) {
        this.volunteerUserService = volunteerUserService;
    }

    @PostMapping("/volunteerUsers")
    VolunteerUserDto newVolunteerUser(@RequestBody VolunteerUserDto volunteerUserDto) {
        if(volunteerUserDto.getId() != null)
            throw new EntityCanNotBeCreatedException("Entity with specified id can not be created");
        VolunteerUser volunteerUser = VolunteerUserConverter.toModel(volunteerUserDto);

        volunteerUserService.create(volunteerUser);
        volunteerUser = volunteerUserService.readById(volunteerUser.getId()).orElseThrow(
                () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
        );

        return volunteerUserDto;
    }

    @GetMapping("/volunteerUsers/{id}")
    VolunteerUserDto getVolunteerUserById(@PathVariable Long id) {
        return VolunteerUserConverter.fromModel(volunteerUserService.readById(id)
                        .orElseThrow(() -> new ResponseStatusException(
                                HttpStatus.NOT_FOUND, "User Not Found")
                        )
        );
    }

    @GetMapping("/volunteerUsers")
    Collection<VolunteerUser> getVolunteers() {
        return volunteerUserService.readAll();
    }


    @PutMapping("/volunteerUsers/{id}")
    VolunteerUserDto updateUser(@RequestBody VolunteerUserDto volunteerUserDto, @PathVariable Long id) {
        volunteerUserService.readById(id)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
                );


        VolunteerUser volunteerUser = VolunteerUserConverter.toModel(volunteerUserDto);
        volunteerUser.setId(id);

        volunteerUserService.update(volunteerUser);
        return volunteerUserDto;

    }

    @DeleteMapping("volunteerUsers/{id}")
    void deleteUser(@PathVariable Long id) {
        if(volunteerUserService.readById(id).isEmpty())
            throw new NoSuchEntityInRepositoryException("Entity with such id is not present");
        volunteerUserService.deleteById(id);
    }

}
