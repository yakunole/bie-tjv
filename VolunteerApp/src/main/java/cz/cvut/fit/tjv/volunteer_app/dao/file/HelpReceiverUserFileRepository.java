package cz.cvut.fit.tjv.volunteer_app.dao.file;

import cz.cvut.fit.tjv.volunteer_app.dao.UserRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.*;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class HelpReceiverUserFileRepository extends AbstractFileRepository<Long, HelpReceiverUser> implements UserRepository {

    protected HelpReceiverUserFileRepository() {
        super(Path.of("helpReceiverUsers.bin"));
    }

    @Override
    public void addFriend(Long id, VolunteerUser newFriend) {
        if (super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.addFriend(newFriend);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    public void addHelpRequest(Long id, HelpRequestPost post) {
        if (super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.addHelpRequest(post);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    public void changeCountry(Long id, Country country) {
        if (super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.changeCountry(country);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    public void changeUsername(Long id, String newUsername) {
        if (super.readById(id).isPresent()) {
            var user = super.readById(id).get();
            user.changeUsername(newUsername);
            super.createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    protected Long entityId(HelpReceiverUser entity) {
        return entity.getId();
    }

    @Override
    public boolean existsById(Long id) {
        return false;
    }
}