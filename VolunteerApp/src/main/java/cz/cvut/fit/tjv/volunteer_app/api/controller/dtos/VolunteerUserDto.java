package cz.cvut.fit.tjv.volunteer_app.api.controller.dtos;

import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Set;

@Getter
@Setter
public class VolunteerUserDto  {
    public Long id;
    public String username;
    public String firstName;
    public String lastName;
    public String country;

    public VolunteerUserDto() {}

    public VolunteerUserDto(String username, String firstName, String lastName, String country) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public VolunteerUserDto(Long id, String username, String firstName, String lastName, String country) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }
}
