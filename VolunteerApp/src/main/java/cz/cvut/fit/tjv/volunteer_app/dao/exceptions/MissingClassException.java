package cz.cvut.fit.tjv.volunteer_app.dao.exceptions;

public class MissingClassException extends RuntimeException {
    public MissingClassException(String msg, Throwable cause) {
        super(msg, cause);
    }
}