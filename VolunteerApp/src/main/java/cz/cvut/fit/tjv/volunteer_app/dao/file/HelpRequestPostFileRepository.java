package cz.cvut.fit.tjv.volunteer_app.dao.file;

import cz.cvut.fit.tjv.volunteer_app.dao.PostRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class HelpRequestPostFileRepository extends AbstractFileRepository<Long, HelpRequestPost> implements PostRepository {

    public HelpRequestPostFileRepository() {
        super(Path.of("helpRequestPosts.bin"));
    }

    @Override
    public void changeContent(Long id, String newContent) {
        if (super.readById(id).isPresent()) {
            var post = super.readById(id).get();
            post.changeContent(newContent);
            super.createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    public void addVolunteer(Long id, VolunteerUser user) {
        if (super.readById(id).isPresent()) {
            var post = super.readById(id).get();
            post.addVolunteer(user);
            super.createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    public void addLike(Long id, VolunteerUser user) {
        if (super.readById(id).isPresent()) {
            var post = super.readById(id).get();
            post.addLike(user);
            super.createOrUpdate(post);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    protected Long entityId(HelpRequestPost entity) {
        return entity.getId();
    }

    @Override
    public boolean existsById(Long id) {
        return false;
    }
}
