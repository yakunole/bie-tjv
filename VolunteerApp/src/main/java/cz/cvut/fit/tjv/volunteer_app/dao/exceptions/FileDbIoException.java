package cz.cvut.fit.tjv.volunteer_app.dao.exceptions;

public class FileDbIoException extends RuntimeException {
    public FileDbIoException(Throwable cause) {
        super(cause);
    }
}