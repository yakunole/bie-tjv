package cz.cvut.fit.tjv.volunteer_app.api.controller.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelpRequestPostDto {
    public Long id;
    public Long authorId;
    public String message;

    public HelpRequestPostDto() {}

    public HelpRequestPostDto(Long id, String message) {
        this.id = id;
        this.message = message;
    }

    public HelpRequestPostDto(String message, Long authorId) {
        this.message = message;
        this.authorId = authorId;
    }
}
