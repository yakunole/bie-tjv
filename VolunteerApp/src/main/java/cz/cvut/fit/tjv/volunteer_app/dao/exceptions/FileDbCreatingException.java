package cz.cvut.fit.tjv.volunteer_app.dao.exceptions;

public class FileDbCreatingException extends RuntimeException {
    public FileDbCreatingException(Throwable cause) {
        super(cause);
    }
}