package cz.cvut.fit.tjv.volunteer_app.domain;

public interface AbstractPost<T> {
    T getContent();
    void changeContent(T newContent);
    void addLike(VolunteerUser user);
}
