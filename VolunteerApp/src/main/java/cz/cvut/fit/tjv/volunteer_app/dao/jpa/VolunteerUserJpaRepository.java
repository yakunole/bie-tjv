package cz.cvut.fit.tjv.volunteer_app.dao.jpa;

import cz.cvut.fit.tjv.volunteer_app.dao.AbstractCrudRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.UserRepository;
import cz.cvut.fit.tjv.volunteer_app.dao.exceptions.NoSuchEntityInRepositoryException;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.HelpRequestPost;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Repository
public interface VolunteerUserJpaRepository extends JpaRepository<VolunteerUser, Long>, AbstractCrudRepository<Long, VolunteerUser>, UserRepository {

    @Override
    default void changeCountry(Long id, Country newCountry) {
        Optional<VolunteerUser> volunteerUser = readById(id);
        if(volunteerUser.isPresent()) {
            var user = volunteerUser.get();
            user.changeCountry(newCountry);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void changeUsername(Long id, String newUsername) {
        Optional<VolunteerUser> volunteerUser = readById(id);
        if(volunteerUser.isPresent()) {
            var user = volunteerUser.get();
            user.changeUsername(newUsername);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void addFriend(Long id, VolunteerUser newFriend) {
        Optional<VolunteerUser> volunteerUser = readById(id);
        if(volunteerUser.isPresent()) {
            var user = volunteerUser.get();
            user.addFriend(newFriend);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    default void addProvidedWork(Long id, HelpRequestPost post) {
        Optional<VolunteerUser> volunteerUser = readById(id);
        if(volunteerUser.isPresent()) {
            var user = volunteerUser.get();
            user.addProvidedWork(post);
            createOrUpdate(user);
        } else {
            throw new NoSuchEntityInRepositoryException("No helpReceiverUser with such id in the database");
        }
    }

    @Override
    default void createOrUpdate(VolunteerUser entity) {
        this.save(entity);
    }

    @Override
    default Optional<VolunteerUser> readById(Long id) {
        return this.findById(id);
    }

    @Override
    default Collection<VolunteerUser> readAll() {
        return this.findAll();
    }

    @Override
    default boolean exists(VolunteerUser entity) {
        return readById(entity.getId()).isPresent();
    }

    @Override
    default boolean existsById(Long id) {
        return readById(Objects.requireNonNull(id)).isPresent();
    }
}
