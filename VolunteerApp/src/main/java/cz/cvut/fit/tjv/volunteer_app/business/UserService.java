package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.domain.AbstractUser;
import cz.cvut.fit.tjv.volunteer_app.domain.Country;
import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;

public interface UserService {
    void changeCountry(Long id, Country country);
    void changeUsername(Long id, String newUsername);
    void addFriend(Long id, VolunteerUser newFriend);
}
