package cz.cvut.fit.tjv.volunteer_app.domain;

public enum Country {
    CzechRepublic("Czech Republic"),
    Germany("Germany"),
    GreatBritain("Great Britain"),
    Ukraine("Ukraine");

    private final String name;

    Country(String name) {
        this.name = name;
    }

    public String getCountryName() {
        return name;
    }
}

