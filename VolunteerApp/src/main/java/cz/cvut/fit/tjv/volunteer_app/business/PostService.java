package cz.cvut.fit.tjv.volunteer_app.business;

import cz.cvut.fit.tjv.volunteer_app.domain.VolunteerUser;

public interface PostService<T> {
    void changeContent(Long id, T newContent);
    void addLike(Long id, VolunteerUser user);
}
