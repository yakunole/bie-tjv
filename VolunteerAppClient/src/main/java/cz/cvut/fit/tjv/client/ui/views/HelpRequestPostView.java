package cz.cvut.fit.tjv.client.ui.views;

import cz.cvut.fit.tjv.client.dto.HelpReceiverUserDto;
import cz.cvut.fit.tjv.client.dto.HelpRequestPostDto;
import org.springframework.stereotype.Component;

@Component
public class HelpRequestPostView {
    public void printUser(HelpRequestPostDto post) {
        System.out.println("Id: " + post.getId());
        System.out.println("Message: " + post.getMessage());
    }
}
