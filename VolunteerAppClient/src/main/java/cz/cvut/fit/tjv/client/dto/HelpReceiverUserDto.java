package cz.cvut.fit.tjv.client.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelpReceiverUserDto {
    public Long id;
    public String username;
    public String firstName;
    public String lastName;
    public String country;

    public HelpReceiverUserDto() {
    }

    public HelpReceiverUserDto(Long id, String username, String firstName, String lastName, String country) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    public HelpReceiverUserDto(String username, String firstName, String lastName, String country) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }
}