package cz.cvut.fit.tjv.client.data;

import cz.cvut.fit.tjv.client.dto.HelpReceiverUserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Component
public class HelpReceiverUserClient {
    private static final String ONE_URI = "/{id}";
    private final WebClient helpReceiverUserWebClient;

    public HelpReceiverUserClient(@Value("${volunteer_app_backend_url}") String backendUrl) {
        helpReceiverUserWebClient = WebClient.create(backendUrl + "/helpReceiverUser");
    }

    public HelpReceiverUserDto createUser(HelpReceiverUserDto user) {
        return helpReceiverUserWebClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .retrieve()
                .bodyToMono(HelpReceiverUserDto.class)
                .block(Duration.ofSeconds(5));
    }

    public HelpReceiverUserDto readUser(Long id) {
        return helpReceiverUserWebClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(HelpReceiverUserDto.class)
                .block();
    }

    public HelpReceiverUserDto updateUser(HelpReceiverUserDto user) {
        return helpReceiverUserWebClient.put()
                .uri(ONE_URI, user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .retrieve()
                .bodyToMono(HelpReceiverUserDto.class)
                .block();
    }

    public void deleteUser(Long id) {
        helpReceiverUserWebClient.delete()
                .uri(ONE_URI, id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }
}
