package cz.cvut.fit.tjv.client.ui.views;

import org.springframework.shell.ExitRequest;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.format.DateTimeParseException;

public class View {
    public void printErrorGeneric(Throwable e) {
        if (e instanceof WebClientRequestException wcre) {
            System.err.println("Cannot connect to Social Network API. Is it accessible at `" + wcre.getUri() + "'?");
            throw new ExitRequest();
        }
        else if (e instanceof DateTimeParseException)
            System.err.println("Invalid date/time format");
        else if (e instanceof WebClientResponseException.InternalServerError)
            System.err.println("Technical server error: ");
        else
            System.err.println("Unknown error. Is Social Network API running?");
    }
    void printErrorCreate(WebClientException e) {
        if (e instanceof WebClientResponseException.Conflict) {
            System.err.println("User with given username already exists.");
        } else
            printErrorGeneric(e);
    }
}
