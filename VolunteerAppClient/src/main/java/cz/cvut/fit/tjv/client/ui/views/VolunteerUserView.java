package cz.cvut.fit.tjv.client.ui.views;

import cz.cvut.fit.tjv.client.dto.VolunteerUserDto;
import org.springframework.stereotype.Component;

@Component
public class VolunteerUserView extends View {
    public void printUser(VolunteerUserDto user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Username: " + user.getUsername());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Country: " + user.getCountry());
    }
}
