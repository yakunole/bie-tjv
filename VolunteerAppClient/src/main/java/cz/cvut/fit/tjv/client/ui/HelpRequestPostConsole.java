package cz.cvut.fit.tjv.client.ui;

import cz.cvut.fit.tjv.client.data.HelpRequestPostClient;
import cz.cvut.fit.tjv.client.dto.HelpReceiverUserDto;
import cz.cvut.fit.tjv.client.dto.HelpRequestPostDto;
import cz.cvut.fit.tjv.client.ui.views.HelpRequestPostView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class HelpRequestPostConsole {
    private final HelpRequestPostClient helpRequestPostClient;
    private final HelpRequestPostView helpRequestPostView;

    public HelpRequestPostConsole(HelpRequestPostClient helpRequestPostClient, HelpRequestPostView helpRequestPostView) {
        this.helpRequestPostClient = helpRequestPostClient;
        this.helpRequestPostView = helpRequestPostView;
    }

    @ShellMethod("Create new helpRequest post")
    public void createHelpRequestPost(String message) {
        var ret = helpRequestPostClient.createUser(new HelpRequestPostDto(message));
        System.out.println("User created");
        helpRequestPostView.printUser(ret);
    }

    @ShellMethod("Retrieve helpRequestPost")
    public void readHelpRequestPost(Long id) {
        var ret = helpRequestPostClient.readUser(id);
        System.out.println("Post retrieved");
        helpRequestPostView.printUser(ret);
    }

    @ShellMethod("Update helpRequest post")
    public void updateHelpRequestPost(String message) {
        var ret = helpRequestPostClient.updateUser(new HelpRequestPostDto(message));
        System.out.println("Post updated");
        helpRequestPostView.printUser(ret);
    }

    @ShellMethod("Help request post deleted")
    public void deleteHelpRequestPost(Long id) {
        helpRequestPostClient.deleteUser(id);
    }

}
