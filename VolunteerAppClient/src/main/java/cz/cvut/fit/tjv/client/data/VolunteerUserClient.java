package cz.cvut.fit.tjv.client.data;

import cz.cvut.fit.tjv.client.dto.VolunteerUserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Component
public class VolunteerUserClient {
    private static final String ONE_URI = "/{id}";
    private final WebClient volunteerUserWebClient;

    public VolunteerUserClient(@Value("${volunteer_app_backend_url}") String backendUrl) {
        volunteerUserWebClient = WebClient.create(backendUrl + "/volunteerUsers");
    }

    public VolunteerUserDto createUser(VolunteerUserDto user) {
        return volunteerUserWebClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .retrieve()
                .bodyToMono(VolunteerUserDto.class)
                .block(Duration.ofSeconds(5));
    }

    public VolunteerUserDto readUser(Integer id) {
        return volunteerUserWebClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(VolunteerUserDto.class)
                .block();
    }

    public VolunteerUserDto updateUser(VolunteerUserDto user) {
        return volunteerUserWebClient.put()
                .uri(ONE_URI, user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .retrieve()
                .bodyToMono(VolunteerUserDto.class)
                .block();
    }

    public void deleteUser(Long id) {
        volunteerUserWebClient.delete()
                .uri(ONE_URI, id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }

}
