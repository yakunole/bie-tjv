package cz.cvut.fit.tjv.client.data;

import cz.cvut.fit.tjv.client.dto.HelpReceiverUserDto;
import cz.cvut.fit.tjv.client.dto.HelpRequestPostDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Component
public class HelpRequestPostClient {
    private static final String ONE_URI = "/{id}";
    private final WebClient helpRequestPostClient;

    public HelpRequestPostClient(@Value("${volunteer_app_backend_url}") String backendUr) {
        helpRequestPostClient = WebClient.create(backendUr + "/helpRequestPost");
    }

    public HelpRequestPostDto createUser(HelpRequestPostDto post) {
        return helpRequestPostClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(post)
                .retrieve()
                .bodyToMono(HelpRequestPostDto.class)
                .block(Duration.ofSeconds(5));
    }

    public HelpRequestPostDto readUser(Long id) {
        return helpRequestPostClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(HelpRequestPostDto.class)
                .block();
    }

    public HelpRequestPostDto updateUser(HelpRequestPostDto post) {
        return helpRequestPostClient.put()
                .uri(ONE_URI, post.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(post)
                .retrieve()
                .bodyToMono(HelpRequestPostDto.class)
                .block();
    }

    public void deleteUser(Long id) {
        helpRequestPostClient.delete()
                .uri(ONE_URI, id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }
}
