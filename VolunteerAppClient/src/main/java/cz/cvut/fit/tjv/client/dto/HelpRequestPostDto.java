package cz.cvut.fit.tjv.client.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HelpRequestPostDto {
    public Long id;
    public String message;

    public HelpRequestPostDto() {}

    public HelpRequestPostDto(Long id, String message) {
        this.id = id;
        this.message = message;
    }

    public HelpRequestPostDto(String message) {
        this.message = message;
    }
}
