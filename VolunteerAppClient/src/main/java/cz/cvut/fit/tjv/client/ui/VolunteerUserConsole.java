package cz.cvut.fit.tjv.client.ui;

import cz.cvut.fit.tjv.client.data.VolunteerUserClient;
import cz.cvut.fit.tjv.client.dto.VolunteerUserDto;
import cz.cvut.fit.tjv.client.ui.views.VolunteerUserView;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

@ShellComponent
public class VolunteerUserConsole {
    private final VolunteerUserClient volunteerUserClient;
    private final VolunteerUserView volunteerUserView;
    private Long currentUserId;

    public VolunteerUserConsole(VolunteerUserClient volunteerUserClient, VolunteerUserView volunteerUserView) {
        this.volunteerUserClient = volunteerUserClient;
        this.volunteerUserView = volunteerUserView;
    }

    public Long getCurrentUserId() {
        return currentUserId;
    }

    public Availability currentUserSetAvailability() {
        return currentUserId == null
                ? Availability.unavailable("current user must be set first")
                : Availability.available();
    }

    @ShellMethod("Select current user")
    public void setCurrentUser(Long id) {
            this.currentUserId = id;
    }

    @ShellMethod("Register new volunteer user account")
    public void createUser(String username, String firstName, String lastName, String country) {
        var ret = volunteerUserClient.createUser(new VolunteerUserDto(username, firstName, lastName, country));
        System.out.println("User created");
        volunteerUserView.printUser(ret);
    }

    @ShellMethod("Retrieve user")
    public void readUser(Integer id) {
        var ret = volunteerUserClient.readUser(id);
        System.out.println("User retrieved");
        volunteerUserView.printUser(ret);
    }

    @ShellMethod("Update user")
    public void updateUser(Long id, String username, String firstName, String lastName, String country) {
        var ret = volunteerUserClient.updateUser(new VolunteerUserDto(id, username, firstName, lastName, country));
        System.out.println("User updated");
        volunteerUserView.printUser(ret);
    }

    @ShellMethod("Delete user")
    public void deleteUser(Long id) {
        volunteerUserClient.deleteUser(id);
    }
}
