package cz.cvut.fit.tjv.client.ui;

import cz.cvut.fit.tjv.client.data.HelpReceiverUserClient;
import cz.cvut.fit.tjv.client.dto.HelpReceiverUserDto;
import cz.cvut.fit.tjv.client.dto.VolunteerUserDto;
import cz.cvut.fit.tjv.client.ui.views.HelpReceiverUserView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class HelpReceiverUserConsole {
    private final HelpReceiverUserClient helpReceiverUserClient;
    private final HelpReceiverUserView helpReceiverUserView;

    public HelpReceiverUserConsole(HelpReceiverUserClient helpReceiverUserClient, HelpReceiverUserView helpReceiverUserView) {
        this.helpReceiverUserClient = helpReceiverUserClient;
        this.helpReceiverUserView = helpReceiverUserView;
    }

    @ShellMethod("Register new helpReceiver user account")
    public void createHelpReceiverUser(String username, String firstName, String lastName, String country) {
        var ret = helpReceiverUserClient.createUser(new HelpReceiverUserDto(username, firstName, lastName, country));
        System.out.println("User created");
        helpReceiverUserView.printUser(ret);
    }

    @ShellMethod("Retrieve helpReceiverUser")
    public void readHelpReceiverUser(Long id) {
        var ret = helpReceiverUserClient.readUser(id);
        System.out.println("User retrieved");
        helpReceiverUserView.printUser(ret);
    }

    @ShellMethod("Update helpReceiverUser")
    public void updateHelpReceiverUser(Long id, String username, String firstName, String lastName, String country) {
        var ret = helpReceiverUserClient.updateUser(new HelpReceiverUserDto(id, username, firstName, lastName, country));
        System.out.println("User updated");
        helpReceiverUserView.printUser(ret);
    }

    @ShellMethod("Delete helpReceiverUser")
    public void deleteHelpReceiverUser(Long id) {
        helpReceiverUserClient.deleteUser(id);
    }
}
